function halfLife(taux) {
    var canvas = $("#expdecay")[0];

    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.font = "20px serif";
    context.strokeStyle = 'black';
    context.strokeText("Exponential decay", 150, 20);

    var x0 = 1;
    var lambda = 1 / taux;

    context.strokeStyle = 'blue';
    context.beginPath();
    context.moveTo(0, 0);
    for (var x = 0; x < canvas.width; x++) {
        var y = Math.exp(-lambda * x) * canvas.height;
        context.lineTo(x, canvas.height - y);
    }

    context.stroke();

    // var lines = 200 ,
    // var frag = canvas.clientWidth / lines; 
    // var scale = canvas.clientHeight ;
    // context.beginPath();
    // context.moveTo(0, scale);
    // context.strokeStyle = '#ff0000';
    // for (var i = 0; i < lines; i++) {
    //     var fn = Math.cos(i / scale * 2) * scale;
    //     context.lineTo(i * frag, -fn + scale);
    // }
    // context.stroke();
}


function currency(taux) {

    var qa = [100], qb = [100], qc = [100];
    var ra = [100], rb = [100], rc = [100];
    qa[0] = 0, qb[0] = 200, qc[0] = 1000;
    var n = 3, ev = 80;
    var c = taux;

    for (var i = 0; i < 100; i++) {
        m = qa[i] + qb[i] + qc[i]
        mn = m / n;

        ra[i] = qa[i] / m;
        rb[i] = qb[i] / m;
        rc[i] = qc[i] / m;

        du = c * mn;
        qa[i + 1] = qa[i] + du;
        qb[i + 1] = qb[i] + du;
        qc[i + 1] = qc[i] + du;
    }


    var canvasr = $("#relatif")[0];
    var contextr = canvasr.getContext("2d");
    contextr.clearRect(0, 0, canvasr.width, canvasr.height);
    contextr.font = "20px serif";
    contextr.strokeStyle = 'black';
    contextr.strokeText("Relative", 150, 20);

    plot(canvasr, ra, 'green');
    plot(canvasr, rb, 'red');
    plot(canvasr, rc, 'blue');

    var canvasq = $("#quantitatif")[0];
    var contextq = canvasq.getContext("2d");
    contextq.clearRect(0, 0, canvasq.width, canvasq.height);
    contextq.font = "20px serif";
    contextq.strokeStyle = 'black';
    contextq.strokeText("Quantitive", 150, 20);
    contextq.stroke();

    var alltimehigh = Math.max(
        Math.max(...qa),
        Math.max(...qb),
        Math.max(...qc),
    )
    for (var i = 0; i < 100; i++) {
        qa[i] /= alltimehigh;
        qb[i] /= alltimehigh;
        qc[i] /= alltimehigh;
    }
    plot(canvasq, qa, 'green');
    plot(canvasq, qb, 'red');
    plot(canvasq, qc, 'blue');
}

function plot(canvas, n = [], color) {
    var context = canvas.getContext("2d");
    var scalex = canvas.width / n.length;
    context.strokeStyle = color;
    context.beginPath();
    context.moveTo(0, 0);
    for (var x = 0; x < 100; x++) {
        var y = n[x] * canvas.height;
        context.lineTo(x * scalex, canvas.height - y);
    }
    context.stroke();
}

function includeSections() {
    var deferred = new $.Deferred();
    var includes = $('[data-include]')
    $.each(includes, function () {
        var file = 'pages/' + $(this).data('include') + '.html'
        $(this).load(file)
    })
    setTimeout(function () {
        deferred.resolve();
    }, 2000)
    return deferred.promise();
}

function addAutoAnimate() {
    var sections = document.getElementsByTagName("section")
    for (const element of sections) {
        element.setAttribute('data-auto-animate', '')
        element.setAttribute('data-auto-animate-easing', 'cubic-bezier(0.770, 0.000, 0.175, 1.000)')
    }
}

function start() {
    var thisSlide = Reveal.getCurrentSlide();
    if (tts.On) {
        // Read the innerText for the listed elements of current slide after waiting 1 second to allow transitions to conclude.
        // setTimeout(function(){tts.ReadVisElmts(thisSlide,"h1","h2","h3","p","li");}, 1000);
        // Read the textContent for the listed elements of the current slide, even hidden ones, after 1 second. In this case the notes class.
        if (tts.readNotes) setTimeout(function () {
            tts.ReadAnyElmts(thisSlide, ".notes");
        }, 1000);
    }
}

