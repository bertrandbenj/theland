var tts = {};
tts.Synth = window.speechSynthesis;

tts.DvIndex = 0;
tts.DvRate = 0.9;
tts.Cancel = true;
tts.readFrags = false;
tts.readNotes = true;

const ttsOn = () => document.getElementById('sound').checked

const voicebyLang = lang => speechSynthesis
    .getVoices()
    .find(voice => voice.lang.startsWith(lang))


const pickVoice = () => {
    var lg = document.getElementsByTagName('html')[0].getAttribute('lang');
    return voicebyLang(lg);
}

const delay = ms => new Promise(res => setTimeout(res, ms));

// FIX A GLITCH 
var myTimeout;
function myTimer() {
    window.speechSynthesis.pause();
    window.speechSynthesis.resume();
    myTimeout = setTimeout(myTimer, 10000);
}

tts.ReadText = function (txt) {
    let utterance = new SpeechSynthesisUtterance(txt);

    utterance.onstart = function (event) {
        myTimer();
    };

    utterance.onend = async function (event) {
        clearTimeout(myTimeout);
        await delay(2000);
        Reveal.next();
    };

    utterance.voice = pickVoice();

    utterance.rate = tts.DvRate;
    tts.Synth.speak(utterance);
};

tts.ReadVisElmts = function () {
    let focusElmt = arguments[0];
    for (let i = 1; i < arguments.length; i++) {
        let xElmts = focusElmt.querySelectorAll(arguments[i]);
        for (let k = 0; k < xElmts.length; k++) {
            tts.ReadText(xElmts[k].innerText);
        }
    }
};

tts.ReadAnyElmts = function () {
    let focusElmt = arguments[0];
    for (let i = 1; i < arguments.length; i++) {
        let xElmts = focusElmt.querySelectorAll(arguments[i]);
        for (let k = 0; k < xElmts.length; k++) {
            tts.ReadText(xElmts[k].textContent);
        }
    }
};

tts.ToggleSpeech = function () {
    if (ttsOn()) {
        tts.ReadText("speech On!")
    } else {
        tts.Synth.cancel();
        tts.ReadText("speech Off!")
    };
};


Reveal.addEventListener('ready', function (event) {
    var thisSlide = Reveal.getCurrentSlide();
    if (ttsOn()) {
        if (tts.readNotes) setTimeout(function () {
            tts.ReadAnyElmts(thisSlide, ".notes");
        }, 1000);
    }
});



Reveal.addEventListener('slidechanged', function (event) {
    var thisSlide = Reveal.getCurrentSlide();
    if (tts.Cancel) tts.Synth.cancel();
    if (ttsOn()) {
        // setTimeout(function(){tts.ReadVisElmts(thisSlide,"h1","h2","h3","p","li");}, 1000);
        if (tts.readNotes) setTimeout(function () { tts.ReadAnyElmts(thisSlide, ".notes"); }, 1000); // Then, conditionally, read hidden notes class.
    }
});

Reveal.addEventListener('fragmentshown', function (event) {
    if (tts.readFrags && ttsOn()) {
        let txt = event.fragment.textContent;
        tts.ReadText(txt);
    }
});


Reveal.configure({
    keyboard: {
        81: function () { tts.Synth.cancel() },
        84: function () {
            var i = document.getElementById('sound');
            i.checked = !i.checked;
            tts.ToggleSpeech();
        }
    }
});
